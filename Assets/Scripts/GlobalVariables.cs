﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVariables : MonoBehaviour
{
	public static int idioma = 0; //0 = portugues, 1 = ingles
	public static int deckEscolhido = 0;

	/*
	public static int[] RedDeck = new int[70];
	public static int[] YellowDeck = new int[70];
	public static int[] GreenDeck = new int[70];
	public static int[] PurpleDeck = new int[70];
*/


	//RESPOSTAS EM 0 OU 1
	//FALSE = 0, TRUE = 1
	public static int[] RedDeckResposta = {1,0,0,1,1,1,0,0,1,0,0,1,1,1,0,0,0,1,0,1,1,0,0,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0,1,1,1,0,0,0,1,0,1,1,1,0,1,1,0,0,1,0,1,1,1,0,1,1,0,0,1,0,1,1,0,0};

	//BLUE = 0, RED = 1
	public static int[] YellowDeckResposta = {1,1,1,0,1,1,0,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,1,0,1,0,0,1,1,0,1,0,1,1,0,0,0,0,0,1,0,0,0,1,0,1,1,1,0,0,1,0,1,0,1,1,1,1,1,0,1,1,1,0,0,0,0,0,1,1};

	//BLUE = 0, RED = 1
	public static int[] GreenDeckResposta = {0,1,1,0,0,1,0,1,1,1,1,1,0,0,0,1,0,1,1,1,0,0,0,0,0,1,0,1,1,0,1,0,0,1,0,0,1,1,1,0,1,1,1,1,0,1,0,1,1,1,0,1,0,1,1,1,1,0,0,0,0,0,1,0,0,1,1,1,0,0};

	//R = 0, P = 1
	public static int[] PurpleDeckResposta = {0,1,0,0,0,1,0,1,0,0,1,0,1,1,1,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,1,1,0,0,0,0,0,0,1,1,0,0,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,1,0,1,0,0,1,0};


	void Start ()
	{

		Debug.Log ("" + RedDeckResposta.Length);
		Debug.Log ("" + YellowDeckResposta.Length);
		Debug.Log ("" + GreenDeckResposta.Length);
		Debug.Log ("" + PurpleDeckResposta.Length);

		/*
		for (int i = 0; i < 70; i++) {
			GreenDeck [i] = i + 1;
			PurpleDeck [i] = i + 1;
			RedDeck [i] = i + 1;
			YellowDeck [i] = i + 1;
		}
		*/
	}

	void Update(){
		SairDoJogo();
	}


	void SairDoJogo(){
			if (Input.GetKeyDown(KeyCode.Escape)) 
				Application.Quit(); 
	}


}

