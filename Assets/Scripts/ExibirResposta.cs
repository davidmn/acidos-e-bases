﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExibirResposta : MonoBehaviour {

	SpriteRenderer rend;
	Sprite spriteresposta;
	public GameObject botaoResposta;
	//bool botaoPressionado = false;


	// Use this for initialization
	void Start () {

		rend = GetComponent<SpriteRenderer> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		//DEIXAR SPRITE TRANSPARENTE
		Color tmp = GetComponent<SpriteRenderer> ().color;
		tmp.a = 0.35f; 

		//DESENHAR SPRITE
		rend.sprite = spriteresposta;

		//DEIXAR TRANSPARENTE
		rend.color = tmp;

//		if (botaoPressionado) {
//			Debug.Log ("chegou aqui1");
//
//			Debug.Log ("chegou aqui2");
//			botaoResposta = null;
//			botaoPressionado = true;
//		}
		
	}

	public void Resposta() {

		GameObject Carta = GameObject.Find ("Carta");
		GeradorDeCarta geradordecarta = Carta.GetComponent<GeradorDeCarta> ();


		switch (GlobalVariables.deckEscolhido) {
		case 1:
			if (GlobalVariables.RedDeckResposta [geradordecarta.ValorAleatorioCarta-1] == 1) {
				Debug.Log ("verdadeiro");

				if (Resources.Load ("Respostas Jogo/Resposta1_True")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta1_True") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}

				//Camera.main.backgroundColor = Color.red;
			} else {
				Debug.Log ("falso");

				if (Resources.Load ("Respostas Jogo/Resposta1_False")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta1_False") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}
			}


			break;
		case 2:
			if (GlobalVariables.YellowDeckResposta [geradordecarta.ValorAleatorioCarta-1] == 1) {
				Debug.Log ("vermelho");
				if (Resources.Load ("Respostas Jogo/Resposta3_Vermelho")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta3_Vermelho") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}
			} else {
				Debug.Log ("azul");
				if (Resources.Load ("Respostas Jogo/Resposta3_Azul")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta3_Azul") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}
			}
			break;
		case 3:
			if (GlobalVariables.GreenDeckResposta [geradordecarta.ValorAleatorioCarta-1] == 1) {
				Debug.Log ("vermelho");
				if (Resources.Load ("Respostas Jogo/Resposta3_Vermelho")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta3_Vermelho") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}
			} else {
				Debug.Log ("azul");
				if (Resources.Load ("Respostas Jogo/Resposta3_Azul")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta3_Azul") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}
			}
			break;
		case 4:
			if (GlobalVariables.PurpleDeckResposta [geradordecarta.ValorAleatorioCarta-1] == 1) {
				Debug.Log ("produto");
				if (Resources.Load ("Respostas Jogo/Resposta2_Produto")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta2_Produto") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}
			} else {
				Debug.Log ("reagente");
				if (Resources.Load ("Respostas Jogo/Resposta2_Reagente")) {
					Texture2D tex = Resources.Load ("Respostas Jogo/Resposta2_Reagente") as Texture2D;
					Sprite spr = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
					spriteresposta = spr;
				}
			}
			break;
		default:
			break;
		}

		botaoResposta.SetActive (false);

	}
}
