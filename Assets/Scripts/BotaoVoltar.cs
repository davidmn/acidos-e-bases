﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotaoVoltar : MonoBehaviour {

	// Use this for initialization
	void Start () {


		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Voltar ();
		}
		
	}

	public void Voltar () {
		// Create a temporary reference to the current scene.
		Scene currentScene = SceneManager.GetActiveScene ();

		// Retrieve the name of this scene.
		string sceneName = currentScene.name;

		if (sceneName == "SceneDeck") {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("SceneMenu");
		}

		if (sceneName == "SceneCarta") {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("SceneDeck");
		}
		
	}

}
