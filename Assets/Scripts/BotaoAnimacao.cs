﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Button))]
public class BotaoAnimacao : MonoBehaviour
{

	public Sprite image_idle;
	public Sprite image_pressed;


	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		

	}

	public void MousePressionado()
	{
		GetComponent<Image> ().overrideSprite = image_pressed;

	}

	public void MouseLevantado()
	{
		GetComponent<Image> ().overrideSprite = image_idle;
	}
}