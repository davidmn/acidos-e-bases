﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScrollScript : MonoBehaviour {

	float scrollSpeed = -0.4f;
	Vector2 startPos;

	// Use this for initialization
	void Start () {
		startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float newPos = Mathf.Repeat (Time.time * scrollSpeed, 11.87f);
		transform.position = startPos + Vector2.right * newPos;
	}
}
