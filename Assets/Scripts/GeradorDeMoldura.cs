﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeradorDeMoldura : MonoBehaviour {

	Sprite spriteMoldura;

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.deckEscolhido) {
		case 1: //Red
			if (Resources.Load ("Cartas/Red/Molde_Vermelho")) {
				Texture2D tex = Resources.Load ("Cartas/Red/Molde_Vermelho") as Texture2D;
				spriteMoldura = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
			}
			break;
		case 2: //Yellow
			if (Resources.Load ("Cartas/Yellow/Molde_Amarelo")) {
				Texture2D tex = Resources.Load ("Cartas/Yellow/Molde_Amarelo") as Texture2D;
				spriteMoldura = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
			}
			break;
		case 3: //Green
			if (Resources.Load ("Cartas/Green/Molde_Verde")) {
				Texture2D tex = Resources.Load ("Cartas/Green/Molde_Verde") as Texture2D;
				spriteMoldura = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
			}
			break;
		case 4: //Purple
			if (Resources.Load ("Cartas/Purple/Molde_Roxo")) {
				Texture2D tex = Resources.Load ("Cartas/Purple/Molde_Roxo") as Texture2D;
				spriteMoldura = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
			}
			break;
		default:
			break;
		}

		GetComponent<SpriteRenderer> ().sprite = spriteMoldura;

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
