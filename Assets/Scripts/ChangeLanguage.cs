﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLanguage : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void MudarLingua() {
		switch (GlobalVariables.idioma) {
		case 0: 
			GlobalVariables.idioma = 1;
			break;
		case 1:
			GlobalVariables.idioma = 0;
			break;
		default:
			GlobalVariables.idioma = 0;
			break;
		}
	}
}
