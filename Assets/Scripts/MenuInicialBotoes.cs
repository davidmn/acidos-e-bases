﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicialBotoes : MonoBehaviour {

	public GameObject CanvasRegras;
	public GameObject CanvasCreditos;
	public GameObject ObjetoAudio;
	AudioSource FontedeAudio;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator DelaySceneLoad()
	{
		FontedeAudio = ObjetoAudio.GetComponent<AudioSource>();
		FontedeAudio.Play(0);
		yield return new WaitForSeconds(0.18f);
		SceneManager.LoadScene("SceneDeck");
	}


	public void ButtonPlay ()
	{
		StartCoroutine ("DelaySceneLoad");
	}



	public void ButtonRegrasAtivar (){

		CanvasRegras.SetActive (true);

	}
		
	public void ButtonCreditosAtivar () {

		CanvasCreditos.SetActive (true);

	}

	public void ButtonImprimir (){
		Application.OpenURL("http://www.ldse.ufc.br/prints.zip");
	}
}
