﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeradorDeCarta : MonoBehaviour
{
	Sprite spritecarta;
	public int ValorAleatorioCarta;

	// Use this for initialization
	void Start ()
	{
		
		ValorAleatorioCarta = Random.Range (0, 70) + 1;
		Debug.Log ("o valor eh: " + (ValorAleatorioCarta));
		Debug.Log ("o deck eh: " + GlobalVariables.deckEscolhido);


		switch (GlobalVariables.deckEscolhido) {
		case 1: //Red
			if (Resources.Load ("Cartas/Red/deck_red_TF-" + ValorAleatorioCarta.ToString ())) {
				Texture2D tex = Resources.Load ("Cartas/Red/deck_red_TF-" + ValorAleatorioCarta.ToString ()) as Texture2D;
				spritecarta = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
				//spritecarta = spr;
			}
			break;
		case 2: //Yellow
			if (Resources.Load ("Cartas/Yellow/deck_yellow_MAP-" + ValorAleatorioCarta.ToString ())) {
				Texture2D tex = Resources.Load ("Cartas/Yellow/deck_yellow_MAP-" + ValorAleatorioCarta.ToString ()) as Texture2D;
				spritecarta = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
				//spritecarta = spr;
			}
			break;
		case 3: //Green
			if (Resources.Load ("Cartas/Green/deck_green_MBS-" + ValorAleatorioCarta.ToString ())) {
				Texture2D tex = Resources.Load ("Cartas/Green/deck_green_MBS-" + ValorAleatorioCarta.ToString ()) as Texture2D;
				spritecarta = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
				//spritecarta = spr;
			}
			break;
		case 4: //Purple
			if (Resources.Load ("Cartas/Purple/deck_purple_RP-" + ValorAleatorioCarta.ToString ())) {
				Texture2D tex = Resources.Load ("Cartas/Purple/deck_purple_RP-" + ValorAleatorioCarta.ToString ()) as Texture2D;
				spritecarta = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
				//spritecarta = spr;
			}
			break;
		default:
			break;
		}

		GetComponent<SpriteRenderer> ().sprite = spritecarta;


	}
	
	// Update is called once per frame
	void Update ()
	{
		

	}
		
}