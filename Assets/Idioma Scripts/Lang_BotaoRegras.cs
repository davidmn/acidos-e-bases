﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_BotaoRegras : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
			switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "REGRAS";
				break;
			case 1:
			GetComponent<Text> ().text = "RULES";
				break;
			default:
			GetComponent<Text> ().text = "REGRAS";
				break;
			}
	}
	
	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "REGRAS";
			break;
		case 1:
			GetComponent<Text> ().text = "RULES";
			break;
		default:
			GetComponent<Text> ().text = "REGRAS";
			break;
	}
}
}
