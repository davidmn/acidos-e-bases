﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_Deck_Purple : MonoBehaviour {
	Sprite spritelogo;
	Texture2D tex;

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {

		case 0: 
			tex = Resources.Load ("deck_purple_RP_PT") as Texture2D;

			spritelogo = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
			break;
		case 1:
			tex = Resources.Load ("deck_purple_RP_EN") as Texture2D;

			spritelogo = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
			break;
		default:
			tex = Resources.Load ("deck_purple_RP_PT") as Texture2D;

			spritelogo = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
			break;


		}
		GetComponent<Image> ().sprite = spritelogo;
	}

	// Update is called once per frame
	void Update () {
//
//		switch (GlobalVariables.idioma) {
//
//		case 0: 
//			tex = Resources.Load ("deck_purple_RP_PT") as Texture2D;
//
//			spritelogo = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
//			break;
//		case 1:
//			tex = Resources.Load ("deck_purple_RP_EN") as Texture2D;
//
//			spritelogo = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
//			break;
//		default:
//			tex = Resources.Load ("deck_purple_RP_EN") as Texture2D;
//
//			spritelogo = Sprite.Create (tex, new Rect (0.0f, 0.0f, tex.width, tex.height), new Vector2 (0.5f, 0.5f), 100.0f);
//			break;
//
//
//		}
//		GetComponent<Image> ().sprite = spritelogo;
	}
}
