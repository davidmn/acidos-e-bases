﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_Creditos2 : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Universidade Federal do Ceará\nDepto. de Química Orgânica e Inorgânica"; 
			break;
		case 1:
			GetComponent<Text> ().text = "Federal University of Ceará\nOrganic and Inorganic Chemistry Department";	
			break;
		default:
			GetComponent<Text> ().text = "Universidade Federal do Ceará\nDepto. de Química Orgânica e Inorgânica";
			break;
		}
		GetComponent<Text> ().fontStyle = FontStyle.Bold;
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Universidade Federal do Ceará\nDepto. de Química Orgânica e Inorgânica";
			break;
		case 1:
			GetComponent<Text> ().text = "Federal University of Ceará\nOrganic and Inorganic Chemistry Department";
			break;
		default:
			GetComponent<Text> ().text = "Universidade Federal do Ceará\nDepto. de Química Orgânica e Inorgânica";
			break;
		}
		GetComponent<Text> ().fontStyle = FontStyle.Bold;
	}
}
