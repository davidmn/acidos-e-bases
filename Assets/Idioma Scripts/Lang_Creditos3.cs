﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_Creditos3 : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Instituto UFC Virtual"; 
			break;
		case 1:
			GetComponent<Text> ().text = "UFC Virtual Institute";
			break;
		default:
			GetComponent<Text> ().text = "Instituto UFC Virtual"; 
			break;
		}
		GetComponent<Text> ().fontStyle = FontStyle.Bold;
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Instituto UFC Virtual"; 
			break;
		case 1:
			GetComponent<Text> ().text = "UFC Virtual Institute";
			break;
		default:
			GetComponent<Text> ().text = "Instituto UFC Virtual"; 
			break;
		}
		GetComponent<Text> ().fontStyle = FontStyle.Bold;
	}
}
