﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_BotaoPlay : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "JOGAR";
			break;
		case 1:
			GetComponent<Text> ().text = "PLAY";
			break;
		default:
			GetComponent<Text> ().text = "JOGAR";
			break;
		}
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "JOGAR";
			break;
		case 1:
			GetComponent<Text> ().text = "PLAY";
			break;
		default:
			GetComponent<Text> ().text = "JOGAR";
			break;
		}
	}
}
