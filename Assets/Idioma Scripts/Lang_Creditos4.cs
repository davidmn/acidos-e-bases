﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_Creditos4 : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Instituto Federal de Educação Ciência e Tecnologia do Ceará"; 
			break;
		case 1:
			GetComponent<Text> ().text = "Federal Institute of Education Science and Tecnology of Ceará";
			break;
		default:
			GetComponent<Text> ().text = "Instituto Federal de Educação Ciência e Tecnologia do Ceará"; 
			break;
		}
		GetComponent<Text> ().fontStyle = FontStyle.Bold;
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Instituto Federal de Educação Ciência e Tecnologia do Ceará"; 
			break;
		case 1:
			GetComponent<Text> ().text = "Federal Institute of Education Science and Tecnology of Ceará";
			break;
		default:
			GetComponent<Text> ().text = "Instituto Federal de Educação Ciência e Tecnologia do Ceará"; 
			break;
		}
		GetComponent<Text> ().fontStyle = FontStyle.Bold;
	}
}
