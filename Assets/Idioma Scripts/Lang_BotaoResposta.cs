﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_BotaoResposta : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "RESPOSTA";
			break;
		case 1:
			GetComponent<Text> ().text = "ANSWER";
			break;
		default:
			GetComponent<Text> ().text = "RESPOSTA";
			break;
		}
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "RESPOSTA";
			break;
		case 1:
			GetComponent<Text> ().text = "ANSWER";
			break;
		default:
			GetComponent<Text> ().text = "RESPOSTA";
			break;
		}
	}
}
