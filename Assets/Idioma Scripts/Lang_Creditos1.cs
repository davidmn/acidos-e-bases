﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_Creditos1 : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Laboratório de Design de Softwares Educacionais\nwww.ldse.ufc.br"; 
			GetComponent<Text> ().fontStyle = FontStyle.Bold;
			break;
		case 1:
			GetComponent<Text> ().text = "Educational Software Design Laboratory\nwww.ldse.ufc.br";
			GetComponent<Text> ().fontStyle = FontStyle.Bold;
			break;
		default:
			GetComponent<Text> ().text = "Laboratório de Design de Softwares Educacionais\nwww.ldse.ufc.br";
			GetComponent<Text> ().fontStyle = FontStyle.Bold;
			break;
		}
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Laboratório de Design de Softwares Educacionais\nwww.ldse.ufc.br";
			GetComponent<Text> ().fontStyle = FontStyle.Bold;
			break;
		case 1:
			GetComponent<Text> ().text = "Educational Software Design Laboratory\nwww.ldse.ufc.br";
			GetComponent<Text> ().fontStyle = FontStyle.Bold;
			break;
		default:
			GetComponent<Text> ().text = "Laboratório de Design de Softwares Educacionais\nwww.ldse.ufc.br";
			GetComponent<Text> ().fontStyle = FontStyle.Bold;
			break;
		}
	}
}
