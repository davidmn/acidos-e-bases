﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_BotaoCreditos : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "CRÉDITOS";
			break;
		case 1:
			GetComponent<Text> ().text = "CREDITS";
			break;
		default:
			GetComponent<Text> ().text = "CRÉDITOS";
			break;
		}
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "CRÉDITOS";
			break;
		case 1:
			GetComponent<Text> ().text = "CREDITS";
			break;
		default:
			GetComponent<Text> ().text = "CRÉDITOS";
			break;
		}
	}
}
