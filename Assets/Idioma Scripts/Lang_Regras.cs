﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lang_Regras : MonoBehaviour {

	// Use this for initialization
	void Start () {

		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Inicialmente, todos os jogadores pegam um conjunto de cartões de resposta e posicionam suas peças na posição inicial rotulada como \"Início\" em seus lados no tabuleiro.\n\nDepois, um jogador vira uma carta do primeiro baralho (vermelho), revelando uma pergunta que deve ser respondida por todos os jogadores, posicionando a carta de resposta virada para baixo no tabuleiro.\n\nQuando todas as cartas de resposta foram posicionadas na placa, todas as cartas de resposta são viradas e a resposta correta é verificada clicando-se no aplicativo novamente. Os jogadores que responderam corretamente avançam suas peças para a próxima casa, enquanto os outros não movem suas peças.\n\nQuando houver jogadores em trilhas diferentes, as cartas dos respectivos baralhos devem ser viradas, mas os jogadores responderão apenas a questão relacionada à sua trilha.\n\nO jogo segue esta dinâmica e termina quando um jogador serve o centro do tabuleiro.\n\nSe mais de um jogador chegar simultaneamente ao centro, estes responderão cartas sucessivas, variando-se o baralho, até que um erre e determine o outro como campeão.";
			break;
		case 1:
			GetComponent<Text> ().text = "Initially, all players receive a set of answer cards and position their playing pieces on the initial position labeled “Start” at their individual side on the board.\n\nAfterwards, a player flips a card from the first questions deck of cards (red) revealing a question that must be answered by all players by positioning one red answer card face down on the board. \n\nWhen all answer cards have been positioned on the board, all answer cards are flipped to face up and the answer is checked. Only the players who answered correctly advance their pieces to next space.\n\nWhen there are pieces in different colored tracks, questions card must be chosen from the corresponding colors questions decks at the same time.\n\nA player must only answer the question of the same color of the track they are on.\n\nThe game follows this pattern and finishes when a player reaches the center of the board.\n\nIf two or more players reach the center of the board at the same time, they will answer questions successively from the four questions decks until only one player hits the answer and becomes the winner.";
			break;
		default:
			GetComponent<Text> ().text = "Inicialmente, todos os jogadores pegam um conjunto de cartões de resposta e posicionam suas peças na posição inicial rotulada como \"Início\" em seus lados no tabuleiro.\n\nDepois, um jogador vira uma carta do primeiro baralho (vermelho), revelando uma pergunta que deve ser respondida por todos os jogadores, posicionando a carta de resposta virada para baixo no tabuleiro.\n\nQuando todas as cartas de resposta foram posicionadas na placa, todas as cartas de resposta são viradas e a resposta correta é verificada clicando-se no aplicativo novamente. Os jogadores que responderam corretamente avançam suas peças para a próxima casa, enquanto os outros não movem suas peças.\n\nQuando houver jogadores em trilhas diferentes, as cartas dos respectivos baralhos devem ser viradas, mas os jogadores responderão apenas a questão relacionada à sua trilha.\n\nO jogo segue esta dinâmica e termina quando um jogador serve o centro do tabuleiro.\n\nSe mais de um jogador chegar simultaneamente ao centro, estes responderão cartas sucessivas, variando-se o baralho, até que um erre e determine o outro como campeão.";
			break;
		}
	}

	// Update is called once per frame
	void Update () {
		switch (GlobalVariables.idioma) {
		case 0: 
			GetComponent<Text> ().text = "Inicialmente, todos os jogadores pegam um conjunto de cartões de resposta e posicionam suas peças na posição inicial rotulada como \"Início\" em seus lados no tabuleiro.\n\nDepois, um jogador vira uma carta do primeiro baralho (vermelho), revelando uma pergunta que deve ser respondida por todos os jogadores, posicionando a carta de resposta virada para baixo no tabuleiro.\n\nQuando todas as cartas de resposta foram posicionadas na placa, todas as cartas de resposta são viradas e a resposta correta é verificada clicando-se no aplicativo novamente. Os jogadores que responderam corretamente avançam suas peças para a próxima casa, enquanto os outros não movem suas peças.\n\nQuando houver jogadores em trilhas diferentes, as cartas dos respectivos baralhos devem ser viradas, mas os jogadores responderão apenas a questão relacionada à sua trilha.\n\nO jogo segue esta dinâmica e termina quando um jogador serve o centro do tabuleiro.\n\nSe mais de um jogador chegar simultaneamente ao centro, estes responderão cartas sucessivas, variando-se o baralho, até que um erre e determine o outro como campeão.";
			break;
		case 1:
			GetComponent<Text> ().text = "Initially, all players receive a set of answer cards and position their playing pieces on the initial position labeled “Start” at their individual side on the board.\n\nAfterwards, a player flips a card from the first questions deck of cards (red) revealing a question that must be answered by all players by positioning one red answer card face down on the board. \n\nWhen all answer cards have been positioned on the board, all answer cards are flipped to face up and the answer is checked. Only the players who answered correctly advance their pieces to next space.\n\nWhen there are pieces in different colored tracks, questions card must be chosen from the corresponding colors questions decks at the same time.\n\nA player must only answer the question of the same color of the track they are on.\n\nThe game follows this pattern and finishes when a player reaches the center of the board.\n\nIf two or more players reach the center of the board at the same time, they will answer questions successively from the four questions decks until only one player hits the answer and becomes the winner.";
			break;
		default:
			GetComponent<Text> ().text = "Inicialmente, todos os jogadores pegam um conjunto de cartões de resposta e posicionam suas peças na posição inicial rotulada como \"Início\" em seus lados no tabuleiro.\n\nDepois, um jogador vira uma carta do primeiro baralho (vermelho), revelando uma pergunta que deve ser respondida por todos os jogadores, posicionando a carta de resposta virada para baixo no tabuleiro.\n\nQuando todas as cartas de resposta foram posicionadas na placa, todas as cartas de resposta são viradas e a resposta correta é verificada clicando-se no aplicativo novamente. Os jogadores que responderam corretamente avançam suas peças para a próxima casa, enquanto os outros não movem suas peças.\n\nQuando houver jogadores em trilhas diferentes, as cartas dos respectivos baralhos devem ser viradas, mas os jogadores responderão apenas a questão relacionada à sua trilha.\n\nO jogo segue esta dinâmica e termina quando um jogador serve o centro do tabuleiro.\n\nSe mais de um jogador chegar simultaneamente ao centro, estes responderão cartas sucessivas, variando-se o baralho, até que um erre e determine o outro como campeão.";
			break;
		}
	}
}
